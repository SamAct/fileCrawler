# -*- coding: utf-8 -*-
"""
Created on Sun Jul 12 03:02:29 2020
Description: File Crawler - a multiprocessed/multi-threaded master code for
copying large number of files from one location to other.

Tested for Windows.
TODO: Add linux capable path identification.
Caution: CPU may shoot to 100% for a while, other task may be impacted.
14/1/2023: Modified to add cli version. Now only files inside folders can be copyed.
Example: 
python file-crawler.py --src "C:\Users\AnuragX\Documents\Python Scripts\mp_file_copy_chtgpt\src" --dest "C:\Users\AnuragX\Documents\Python Scripts\mp_file_copy_chtgpt\dest"

@author: Sam Act
"""

import argparse
import shutil
import multiprocessing
import os
from threading import Event

def copy_instance(file, child_file): 

    os.system('color')
    dark_green = "\033[0;32m"
    END = '\033[0m'
    # Copy file as child_file at destination.
    try:
        shutil.copy(file, child_file)
    except FileExistsError:
        print("Directory " , child_file ,  " already exists, skipping!")
    # Multiprocessing ID process.
    print(dark_green + "Copying file: " + str(file.replace("/", "\\").split("\\")[-1]) +". ID of main process: {}".format(os.getpid()) + END) 

def copy_instance2(dest, subdir, src): 
    # Replicate Folders at the destination.
    try:
        os.mkdir(dest + subdir.split(src)[-1])
        #os.makedirs(dest + subdir.split(src)[-1], exist_ok = True)
        print("Creating folder: " + (dest + subdir.split(src)[-1]))
    except FileExistsError:
        print("Directory " , (dest + subdir.split(src)[-1]) ,  " already exists, skipping.")

def copy_files_parallel(src, dest):
    for dest_subdir, dest_dir, dest_files in os.walk(src):
        if dest_subdir != src:
            for ith in dest_files:
                file = (str(dest_subdir) + '\\' + str(ith))
                child_file = str(dest) + (str(dest_subdir) + '\\' + str(ith)).split(src)[-1]
                p2 = multiprocessing.Process(target=copy_instance, args=(file, child_file))
                p2.start()
                p3 = multiprocessing.Process(target=copy_instance2, args=(dest, dest_subdir, src))
                p3.start()
        if dest_subdir == src:
            for ith in dest_files:
                file = (str(dest_subdir) + '\\' + str(ith))
                child_file = str(dest) + (str(dest_subdir) + '\\' + str(ith)).split(src)[-1]
                p2 = multiprocessing.Process(target=copy_instance, args=(file, child_file))
                p2.start()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Copy files from source to destination in parallel')
    parser.add_argument('--src', help='Source directory')
    parser.add_argument('--dest', help='Destination directory')
    args = parser.parse_args()
    src = args.src
    dest = args.dest
    copy_files_parallel(src, dest)

# FileCrawler: A Multiprocessed File Coping Tool

## Introduction
Move away from traditional file copying in Windows or Linux, use multiprocessor file copying, that copies multiple files parallelly. 

Coping Large number of files from Source to Destination made easy.

## Installation

```python
#Install pre-requisites,
pip install argparse, shutil, multiprocessing, os, threading
# Clone this repo
git clone https://gitlab.com/SamAct/fileCrawler.git
```

## Usage
```python
python file-crawler.py --src "path_to_source_directory" --dest "path_to_destination_directory"
```
Warning: High CPU usage.

## License
This is a open source project with MIT licensed.
